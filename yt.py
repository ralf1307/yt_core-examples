#!/usr/bin/env python3
import sys
from yt.Application import Application

if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
