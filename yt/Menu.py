import gi
from gi.repository import Gtk

@Gtk.Template(filename='data/MenuPopover.ui')
class Menu(Gtk.Popover):
    __gtype_name__ = "Menu"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
