import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Handy", "1")
from gi.repository import Gtk, Gio, GLib

from yt.MainWindow import MainWindow as App_MainWindow
from yt.About import App_About

class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="de.ralfrachinger.yt",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            **kwargs
        )
        self.win = None
        
        self.add_main_option(
                    "search",
                    ord("s"),
                    GLib.OptionFlags.NONE,
                    GLib.OptionArg.NONE,
                    "Opens the Search-Window.",
                    None
                )
        self.add_main_option(
                    "about",
                    ord("a"),
                    GLib.OptionFlags.NONE,
                    GLib.OptionArg.NONE,
                    "Opens the About-Dialog",
                    None
                )
        

    def do_startup(self):
        Gtk.Application.do_startup(self)
        # FIXME: Translation for languages like Japanese needed?
        GLib.set_application_name("HandyYT")
        
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        # GVariant didnt work for me well on Python. 
        action = Gio.SimpleAction.new("search", None)
        action.connect("activate", self.on_search)
        self.add_action(action)
        
    def do_activate(self):
        if type(self.win) == type(None):
            self.win = App_MainWindow(application=self, title="Main Window")
            self.win.show_all()

    
    def on_about(self, action, param):
        about_dialog = App_About(transient_for=self.win, modal=True)
        about_dialog.present()

    def on_search(self, action, param):
        if type(self.win) == type(None):
            self.activate()
        # If someone activtates the action we just toggle guiSearchButton()
        self.win.guiSearchButton()
    
    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()
        
        if "search" in options:
            self.activate_action("search", None)
        if "about" in options:
            self.activate_action("about", None)

        self.activate()
