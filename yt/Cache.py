import threading, time
import os

class Cache:

    # array of str
    files = []

    # array of all the things ;)
    memory = []
    
    # array of GdkPixbufs
    pixbufs = []

    def add_file(self, path=str()):
        if path != "":
            self.files.append(path)

    def __del__(self):
        # Should we multithread this?
        for f in self.files:
            os.remove(f)
        for m in self.memory:
            del m
        for pixbuf in self.pixbufs:
            pixbuf.destroy()
    
    # Queue code
    def __init__(self):
        thread = threading.Thread(target=self.work_queue)
        thread.daemon = True
        thread.start()


    thumbnail_queue = []
    
    def work_queue(self):
        while(True):
            if len(self.thumbnail_queue) > 0:
                self.thumbnail_queue.pop(0)()
            time.sleep(0.1)

    def append_queue_thumbnail(self, thread):
        self.thumbnail_queue.append(thread)
