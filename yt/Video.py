import gi

from gi.repository import Gtk

import yt_core
import youtube_dl

from .Item import ItemMedia

@Gtk.Template(filename='data/Video.ui')
class Video(Gtk.Box):
    __gtype_name__ = "VideoMobile"

    videoBox = Gtk.Template.Child()
    videoAdditionalStack = Gtk.Template.Child()
    recommendedListBox = Gtk.Template.Child()
    commentsListBox = Gtk.Template.Child()
    
    autoplayBox = Gtk.Template.Child()

    metadataTitleLabel = Gtk.Template.Child()
    metadataInfo = Gtk.Template.Child()
    metadataDescription = Gtk.Template.Child()

    commentsListBoxRowSizeGroup = Gtk.Template.Child()
    recommendedListBoxRowSizeGroup = Gtk.Template.Child()
    recommendedListBoxRowImageSizeGroup = Gtk.Template.Child()

    ytCore = None
    ytCoreVideo = None
    store = None
    watch_id = ""

    def __init__(self, store, watch_id=str()):
        super().__init__()
        self.watch_id = watch_id
        self.store = store
        self.ytCore = self.store.yt_core_core
        self.connect("realize", self.realized)

    def realized(self, *args):
        self.ytCoreVideo = self.ytCore.new_video(self.watch_id)
        metadata = self.ytCoreVideo.get_metadata()
        self.metadataTitleLabel.props.label = metadata["title"]
        self.metadataInfo.props.label = self.metadata(metadata)
        #self.metadataDescription.props.label = metadata["description"]
        self.recommended()
        # comments not yet implemented in yt_core
        self.comments()

    def recommended(self):
        if self.ytCoreVideo.is_autoplay_available():
            autoplay = self.ytCoreVideo.get_autoplay()
            item = ItemMedia(autoplay, self.store.cache, self.recommendedListBoxRowImageSizeGroup, self.recommendedListBox)
            self.recommendedListBoxRowSizeGroup.add_widget(item)
            self.autoplayBox = item
        if self.ytCoreVideo.is_recommended_available():
            recommended_list = self.ytCoreVideo.get_recommended()
            for rec in recommended_list:
                item = ItemMedia(rec, self.store.cache, self.recommendedListBoxRowImageSizeGroup, self.recommendedListBox)
                self.recommendedListBoxRowSizeGroup.add_widget(item)
                self.recommendedListBox.add(item) 

    def comments(self):
        pass
    
    @staticmethod
    def metadata(data):
        def text(str1, str2):
            if str1:
                return str1 + " - " + str2
            else: return str2

        out = ""
        #lengthText = data["lengthText"]
        viewCountText = data["likeDislikeText"]
        owner = data["owner"]
        #out = text(out, lengthText)
        out = text(out, viewCountText)
        out = text(out, owner["title"])
        return out
