import gi

from gi.repository import Gtk, Gio
from gi.repository import Handy

from yt.Search import SearchBox
from yt.Video import Video
from yt.Menu import Menu

from yt.Cache import Cache

import yt_core

class Store:
    cache = Cache()
    yt_core_core = None
    action_video = None

@Gtk.Template(filename='data/MainWindow.ui')
class MainWindow(Handy.ApplicationWindow):
    
    __gtype_name__ = "MainWindow"

    mainStack = Gtk.Template.Child()

    userStack = Gtk.Template.Child()
    userSwitchTitle = Gtk.Template.Child()
    userSwitchBar = Gtk.Template.Child()

    headerButtonSearch = Gtk.Template.Child()
    headerButtonMenu = Gtk.Template.Child()

    store = Store()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs) 
        self.store.yt_core_core = yt_core.Core()
        self.store.action_video = self.action_video
        # Sets Menu
        menu = Menu()
        self.headerButtonMenu.set_popover(menu)
        # Search Button
        self.headerButtonSearch.connect("released", self.guiSearchButton)
        # Connecting destroy signal
        self.connect("destroy", Gtk.main_quit)

    searchPage = False
    searchPageVisible = False
    searchLastVisible = None

    def guiSearchButton(self, *args):
        if not self.searchPage:
            self.addSearch()
        # THIS WILL CAUSE A MEMORY LEAK, aka dont abuse a button
        # it's going to seek revenge
        # .
        # the buttons have feelings too, you know.
        if self.searchPageVisible:
            self.mainStack.set_visible_child(self.searchLastVisible)
            self.searchPageVisible = False
        else:
            self.searchLastVisible = self.mainStack.get_visible_child()
            self.mainStack.set_visible_child_name("searchPage")
            self.searchPageVisible = True

    def addSearch(self):
        self.searchBox = SearchBox(self.store)
        self.addSearchToStack()
        self.searchPage = True

    def addSearchToStack(self):
        self.searchBox.show()
        self.mainStack.add_named(self.searchBox, "searchPage")

    def updateSubtitle(self, new_title=str()):
        pass

    videoPage = None

    def action_video(self, videoid=str()):
        self.videoPage = self.userStack.get_child_by_name("video")
        if type(self.videoPage) != type(None):
            self.videoPage.destroy()
        self.videoPage = Video(self.store, videoid)
        self.videoPage.show()
        self.userStack.add_named(self.videoPage, "video")
        self.userStack.set_visible_child_name("video")
