import gi

from gi.repository import Gtk

import yt_core
from .Item import ItemMedia

import threading, time
# FIXME: Use another threads for getting search results from yt_core
@Gtk.Template(filename='data/Search.ui')
class SearchBox(Gtk.Box):
    __gtype_name__ = "SearchBox"
    
    searchListBox = Gtk.Template.Child()
    searchEntry = Gtk.Template.Child()
    searchButton = Gtk.Template.Child()
    searchWindowAdjustment = Gtk.Template.Child()
    searchScrolledWindow = Gtk.Template.Child()
    searchListBoxRowSizeGroup = Gtk.Template.Child()
    searchListBoxRowImageSizeGroup = Gtk.Template.Child()
    
    hasData = False
    forceKeepResult = False

    store = None

    def __init__(self, store):
        super().__init__()
        self.connect("unrealize", self.guiHidden)
        self.searchButton.connect("released", self.guiSearchGo)
        self.searchListBox.connect("row-selected", self.row_selected)
        self.store = store
        self.core = store.yt_core_core
        self.coreSearch = self.core.new_search()
        self.cache = store.cache
        thread = threading.Thread(target=self.act)
        thread.daemon = True
        thread.start()
    
    items_with_action_on_visible = []

    def search(self, searchquery):
        self.hasData = True
        self.coreSearch.searchterm = searchquery
        self.coreSearch.search()
        for i in self.coreSearch.get_results():
            if i["type"] == "video":
                item = ItemMedia(i, self.cache, self.searchListBoxRowImageSizeGroup, self)
                self.searchListBoxRowSizeGroup.add_widget(item)
                self.items_with_action_on_visible.append(item)
                self.searchListBox.add(item)


    def cleanup(self):
        if self.hasData:
            for i in self.searchListBox.get_children():
                i.destroy()
            self.hasData = False

    def guiSearchGo(self, *args):
        self.cleanup()
        self.search(self.searchEntry.get_text())

    def guiHidden(self, *args):
        if not self.forceKeepResult:
            self.cleanup()

    def act(self):
        while(True):
            for row in self.items_with_action_on_visible:
                if row.thumbnailShown:
                    self.items_with_action_on_visible.remove(row)
                else:
                    row.act()
            time.sleep(0.3)

    def row_selected(self, box, row, *args):
        if not type(row) is type(None):
            childs = row.get_children() 
            for child in childs:
                self.store.action_video(child.data["Id"])
