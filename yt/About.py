import gi

from gi.repository import Gtk, GdkPixbuf

class App_About(Gtk.AboutDialog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_version("Alpha 0.0.0.0.01")
        self.set_website("https://ralfrachinger.de/handyyt")
        self.set_license_type(Gtk.License.AGPL_3_0)
        self.set_copyright("(c) Ralf Rachinger 2021")
        self.set_authors(self.author_str)
        
        self.set_opacity(0.9)


    author_str = '''Ralf Rachinger (yt_core)\n\
                    Ralf Rachinger (HandyYT)'''
